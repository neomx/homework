package com.expeditors.db;

import com.expeditors.domain.Breeds;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.sql.DataSource;

public class jdbcAdoptService {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5433/adoptservice";
        String user = "larku";
        String pw = "larku";

        try (Connection connection = DriverManager.getConnection(url, user, pw)) {
            //getAllBreeds(connection);
            //insertBreed(connection);
            //insertPetType(connection);
            //insertPet(connection);
            insertAdopter(connection);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);

        }

    }

    public static void insertBreed(Connection connection) {
        String sql = "insert into breeds (breed_type) values (?)";

        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "Snapping Turtle");

            int rows = ps.executeUpdate();

            System.out.println(rows);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }

        /*
        List<Integer> newKeys = new ArrayList<>();
        int rowsAffected = 0;

        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "Snapping Turtle");

            rowsAffected += ps.executeUpdate();

            try (var keys = ps.getGeneratedKeys();) {
                keys.next();
                var newId = keys.getInt(1);
                newKeys.add(newId);
            }
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println("rowsAffected: " + rowsAffected + ", newKeys: " + newKeys);
         */
    }


    public static void getAllBreeds(Connection connection) {
        String query = "select * from breeds";

        try (PreparedStatement ps = connection.prepareStatement(query);) {

            ResultSet rs = ps.executeQuery();

            List<Breeds> breeds = new ArrayList<>();
            while (rs.next()) {
                String breedType = rs.getString("breed_type");
                int id = rs.getInt("breed_id");

                // Create java object from queried data
                Breeds breed = new Breeds(id, "");
                breed.setId(id);
                breed.setBreedType(breedType);

                breeds.add(breed);
            }

            breeds.forEach(System.out::println);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public static void insertPetType(Connection connection) {
        String sql = "insert into public.pet_types (pet_type, breed_id) values (?, ?)";

        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "Turtle");
            ps.setInt(2, 9);

            int rows = ps.executeUpdate();

            System.out.println(rows);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void insertPet(Connection connection) {
        String sql = "insert into public.pets (pet_name, pet_type_id) values (?, ?)";

        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "Leo");
            ps.setInt(2, 3);

            int rows = ps.executeUpdate();

            System.out.println(rows);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void insertAdopter(Connection connection) {
        String sql = "insert into public.adopters (first_name, last_name, adoption_date, pet_id) values (?, ?, ?, ?)";

        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "Gwen");
            ps.setString(2, "Torres");
            ps.setDate(3, java.sql.Date.valueOf("2024-05-01"));
            ps.setInt(4, 1);

            int rows = ps.executeUpdate();

            System.out.println(rows);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
