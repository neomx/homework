package com.expeditors.db;

import com.expeditors.domain.Breeds;
import com.expeditors.domain.PetTypes;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.expeditors.domain.Breeds;

public class jdbcAdoptServiceTemplate {
    public static void main(String[] args) {
        jdbcAdoptServiceTemplate jast = new jdbcAdoptServiceTemplate();
        String url = "jdbc:postgresql://localhost:5433/adoptservice";
        String user = "larku";
        String pw = "larku";
        DriverManagerDataSource dataSource = new DriverManagerDataSource(url, user, pw);

        JdbcTemplate template = new JdbcTemplate(dataSource);
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

        //jast.getBreedWithOneColumn(template);
        //jast.getBreedWithAllColumns(template);
        jast.getAllRowsFromBreedsTable(template);
        //jast.insertPetType(template);
        //jast.getAllRowsFromPetTypesTable(template);

    }

    // Return the breed type for one breed
    public void getBreedWithOneColumn(JdbcTemplate template) {
        String sql ="select breed_type from breeds where breed_id = ?";
        String breedType = template.queryForObject(sql, String.class, 6);
        System.out.println("breed type: " + breedType);
    }

    // Return all columns for one breed.
    public void getBreedWithAllColumns(JdbcTemplate template) {
        String sql = "select * from breeds where breed_id = ?";
        RowMapper<Breeds> rowMapper = (rSet, rowNum) -> {
            int id = rSet.getInt("breed_id");
            String breedType = rSet.getString("breed_type");

            var newObj = new Breeds(id, breedType);
            newObj.setId(id);
            return newObj;
        };

        Breeds breeds = template.queryForObject(sql, rowMapper, 6);
        System.out.println("Breed: " + breeds);

    }

    // Return all rows in the breeds table
    public void getAllRowsFromBreedsTable(JdbcTemplate template) {
        String sql = "select * from breeds";
        RowMapper<Breeds> rowMapper = (rSet, rowNum) -> {
            int id = rSet.getInt("breed_id");
            String breedType = rSet.getString("breed_type");

            var newObj = new Breeds(id, breedType);
            newObj.setId(id);
            return newObj;
        };

        List<Breeds> breeds = template.query(sql, rowMapper);
        System.out.println("Breeds: " + breeds);

    }

    // Insert a record to the pet_types table
    public void insertPetType(JdbcTemplate template) {
        String insertPetTypeSQL = "insert into pet_types (pet_type, breed_id) values (?, ?)";

        Object [] arr = new Object[]{"DOG", 5};
        List<Object[]> params = new ArrayList<>();
        params.add(arr);
        int numRows = 0;
        for (Object[] args : params) {
            numRows += template.update(insertPetTypeSQL, args);
        }
        System.out.println("numRows: " + numRows);

    }

    // Return all rows in the pet_types table
    public void getAllRowsFromPetTypesTable(JdbcTemplate template) {
        String sql = "select * from pet_types";
        RowMapper<PetTypes> rowMapper = (rSet, rowNum) -> {
            int id = rSet.getInt("pet_type_id");
            String petType = rSet.getString("pet_type");

            var newObj = new PetTypes(id, petType);
            newObj.setId(id);
            return newObj;
        };

        List<PetTypes> pettypes = template.query(sql, rowMapper);
        System.out.println("PetTypes: " + pettypes);
    }

}

class MyRowMapper implements RowMapper<String>{

    @Override
    public String mapRow(ResultSet rs, int rowNum) throws SQLException {
        return null;
    }
}
