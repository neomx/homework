package com.expeditors.domain;

public class PetTypes {
    private int id;
    private String petType;

    public PetTypes(int id, String petType) {
        this.id = id;
        this.petType = petType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    @Override
    public String toString() {
        return "PetTypes [id=" + id + ", type=" + petType + "]";
    }
}
