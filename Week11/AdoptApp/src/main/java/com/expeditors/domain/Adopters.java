package com.expeditors.domain;

import java.time.LocalDate;

public class Adopters {
    private int id;
    private String firstName;
    private String lastName;
    private LocalDate adoptionDate;

    public Adopters(int id, String firstName, String lastName, LocalDate adoptionDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.adoptionDate = adoptionDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getAdoptionDate() {
        return adoptionDate;
    }

    public void setAdoptionDate(LocalDate adoptionDate) {
        this.adoptionDate = adoptionDate;
    }

    @Override
    public String toString() {
        return "Adopter [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", adoptionDate=" + adoptionDate + "]";
    }
}
