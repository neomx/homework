package com.expeditors.domain;

public class Pets {
    private int id;
    private String petName;

    public Pets(int id, String petName) {
        this.id = id;
        this.petName = petName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    @Override
    public String toString() {
        return "Pet [id=" + id + ", type=" + petName + "]";
    }
}
