package com.expeditors.domain;

import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;
import static jakarta.persistence.CascadeType.PERSIST;

@Table(name = "breeds")
@Entity
public class Breeds {
    @Id
    @GeneratedValue
    @Column(name = "breed_id")
    private int id;

    @Column(name = "breed_type", length = 15)
    private String breedType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBreedType() {
        return breedType;
    }

    public void setBreedType(String breedType) {
        this.breedType = breedType;
    }

    public Breeds() {

    }

    public Breeds(int id, String breedType) {
        this.id = id;
        this.breedType = breedType;
    }

    public Breeds(String breedType) {
        this.breedType = breedType;
    }

    @Override
    public String toString() {
        return "Breed [id=" + id + ", type=" + breedType + "]";
    }
}
