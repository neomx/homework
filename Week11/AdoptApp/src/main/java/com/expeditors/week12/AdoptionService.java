package com.expeditors.week12;

import com.expeditors.domain.Breeds;
import jakarta.persistence.EntityManager;

public class AdoptionService {
    private EntityManager em;

    public AdoptionService(EntityManager em) {
        this.em = em;
    }

    public Breeds createBreed(Breeds breed) {
        em.persist(breed);
        return breed;
    }

    public Breeds findBreed(int id) {
        return em.find(Breeds.class, id);
    }

    public void removeBreed(Breeds breeds) {
        em.remove(em.merge(breeds));
    }

}
