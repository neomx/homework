package com.expeditors.week12;

import java.sql.SQLException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import com.expeditors.domain.Breeds;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("week12-persistence-unit");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        AdoptionService service = new AdoptionService(em);
        Breeds breeds = new Breeds("Python");

        // Create a new breed
        /*
        tx.begin();
        breeds = service.createBreed(breeds);
        tx.commit();
        int id = breeds.getId();

        System.out.println("Breed Persisted: " + breeds);
         */

        // Find a breed
        /*
        breeds = service.findBreed(9);
        System.out.println("Breed Found : " + breeds);
         */


        // Removes a breed
        breeds = service.findBreed(-24);
        tx.begin();
        breeds = service.createBreed(breeds);
        service.removeBreed(breeds);
        tx.commit();

        System.out.println("Breed Removed");

        em.close();
        emf.close();

    }
}
