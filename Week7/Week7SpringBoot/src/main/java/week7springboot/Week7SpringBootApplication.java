package week7springboot;

import com.expeditors.domain.Adopter;
import com.expeditors.service.AdopterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

//@SpringBootApplication
@Configuration
@ComponentScan({"com.expeditors", "week7springboot"})
@EnableAutoConfiguration
public class Week7SpringBootApplication {

    public static void main(String[] args) {

        SpringApplication.run(Week7SpringBootApplication.class, args);
    }
}

@Component
class MyRunner implements CommandLineRunner {
    @Autowired
    private AdopterService adopterService;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Here we go with Spring Boot");

        List<Adopter> adopters = adopterService.getAllAdopters();
        System.out.println(adopters.size());
        System.out.println(adopters);
    }
}
