package com.expeditors.jconfig;

import com.expeditors.dao.AdopterDAO;
import com.expeditors.dao.inmemory.InMemoryAdopterDAO;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource({"classpath:/backend.properties"})
@ComponentScan("com.expeditors")
public class AppConfig {
    private AppTestDataConfig testDataProducer = new AppTestDataConfig();

    @Bean
    public AdopterDAO adopterDAO() {
        var dao = new InMemoryAdopterDAO();
        dao.insert(testDataProducer.adopter1());
        dao.insert(testDataProducer.adopter2());
        return dao;
    }
}
