--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO breeds VALUES (1, 'Pitbull');
INSERT INTO breeds VALUES (2, 'Maltese');
INSERT INTO breeds VALUES (3, 'Siamese');
INSERT INTO breeds VALUES (4, 'Persian');
INSERT INTO breeds VALUES (5, 'Green Turtle');
INSERT INTO breeds VALUES (6, 'Snapping Turtle');


INSERT INTO pet_types VALUES (1, 'Dog',1);
INSERT INTO pet_types VALUES (2, 'Dog',2);
INSERT INTO pet_types VALUES (3, 'Cat',3);
INSERT INTO pet_types VALUES (4, 'Cat',4);
INSERT INTO pet_types VALUES (5, 'Turtle',5);
INSERT INTO pet_types VALUES (6, 'Turtle',6);


INSERT INTO pets VALUES (1, 'Rocky',1);
INSERT INTO pets VALUES (2, 'Cosmo',2);
INSERT INTO pets VALUES (3, 'MeowMeow',3);
INSERT INTO pets VALUES (4, 'Lady',4);
INSERT INTO pets VALUES (5, 'Donatello',5);
INSERT INTO pets VALUES (6, 'Leo',6);

INSERT INTO adopters VALUES (1, 'Mary','Jones','2024-01-01',1);
INSERT INTO adopters VALUES (2, 'Tom','Fields','2024-02-01',2);
INSERT INTO adopters VALUES (3, 'Denver','Beil','2024-03-01',3);
INSERT INTO adopters VALUES (4, 'Gwen','Torres','2024-03-12',4);
INSERT INTO adopters VALUES (5, 'Jayson','Lu','2024-04-10',5);
INSERT INTO adopters VALUES (6, 'Oscar','Mills','2024-05-01',6);


