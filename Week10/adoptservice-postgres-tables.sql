--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET default_tablespace = '';

SET default_with_oids = false;


---
--- drop tables
---


DROP TABLE IF EXISTS breeds;
DROP TABLE IF EXISTS pet_types;
DROP TABLE IF EXISTS pets;
DROP TABLE IF EXISTS adopters;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE breeds (
    int breed_id NOT NULL PRIMARY KEY,
    breed_type VARCHAR(15) NOT NULL
);

/*
CREATE TABLE pet_types (
    int pet_type_id NOT NULL PRIMARY KEY,
    pet_type VARCHAR(15) NOT NULL,
	breed_id int NOT NULL,
	FOREIGN KEY (breed_id) REFERENCES breeds(breed_id)
);


CREATE TABLE pets (
    pet_id int NOT NULL PRIMARY KEY,
    pet_name VARCHAR(15) NOT NULL,
	pet_type_id int NOT NULL,
	FOREIGN KEY (pet_type_id) REFERENCES pet_types(pet_type_id)
);


CREATE TABLE adopters (
    adopter_id int NOT NULL PRIMARY KEY,
    first_name VARCHAR(15) NOT NULL,
	last_name VARCHAR(15) NOT NULL,
	adoption_date date,
	pet_id int NOT NULL,
	FOREIGN KEY (pet_id) REFERENCES pets(pet_id)
);
*/



