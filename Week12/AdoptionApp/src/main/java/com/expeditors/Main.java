package com.expeditors;

import java.sql.SQLException;

import com.expeditors.domain.PetType;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import com.expeditors.domain.Breed;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
        System.out.println("Hello and welcome!");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("week12-persistence-unit");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        AdoptService service = new AdoptService(em);

        tx.begin();
        // Create a pet type
        PetType petType1 = new PetType();
        petType1.setPetType("Dog");

        // Store the pet type
        em.persist(petType1);
        tx.commit();

        // Create pet type list
        List<PetType> petlist = new ArrayList<>();
        petlist.add(petType1);

        // Create a breed
        Breed breed = new Breed();
        breed.setBreedType("Maltese");
        breed.setPetTypeList(petlist);

        // Store the breed
        tx.begin();
        em.persist(breed);
        tx.commit();

        //em.getTransaction().commit();
        em.close();

        /*
        Set<PetType> animalPet = new HashSet<>();
        animalPet.add(new PetType("Dog"));

        Breed animalBreed = new Breed("Maltese");

        animalBreed.setPetTypes(animalPet);
        animalBreed = service.createBreed(animalBreed);
        tx.commit();
         */

        //System.out.println("Breed Persisted: " + animalBreed);

    }
}