package com.expeditors.domain;

import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import com.expeditors.domain.PetType;
import static jakarta.persistence.CascadeType.PERSIST;

@Table(name = "breed")
@Entity
public class Breed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "breed_id")
    private int id;

    @Column(name = "type", length = 15)
    private String breedType;

    //@OneToMany(cascade = PERSIST)
    //@OneToMany(targetEntity = PetType.class)
    @OneToMany(mappedBy = "breed")
    //@JoinColumn
    //private Set<PetType> petTypes = new HashSet<>();
    private List<PetType> petTypeList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBreedType() {
        return breedType;
    }

    public void setBreedType(String breedType) {
        this.breedType = breedType;
    }

    public Breed() {}

    public Breed(String breedType) {
        this.breedType = breedType;
    }

    public List getPetTypeList() {
        return petTypeList;
    }

    public void setPetTypeList(List petTypeList) {
        this.petTypeList = petTypeList;
    }

    /*
    public void setPetTypes(Set<PetType> petType) {
        this.petType = petTypes;
    }
    */
    @Override
    public String toString() {
        return "Breed [id=" + id + ", type=" + breedType + "]";
    }
}
