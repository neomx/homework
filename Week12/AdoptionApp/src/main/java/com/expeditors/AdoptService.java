package com.expeditors;

import jakarta.persistence.EntityManager;
import com.expeditors.domain.Breed;


public class AdoptService {
    private EntityManager em;

    public AdoptService(EntityManager em) {
        this.em = em;
    }

    public Breed createBreed(Breed breed) {
        em.persist(breed);
        return breed;
    }

    public void removeBreed(int id) {
        Breed breed = em.find(Breed.class, id);
        if (breed != null)
            em.remove(breed);
    }

    public Breed findBreed(int id) {
        return em.find(Breed.class, id);
    }
}
