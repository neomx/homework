package com.expeditors.demo2.domain;

import jakarta.persistence.*;

@Table(name = "pet_type")
@Entity
public class PetType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "type")
    private String petType;

    public PetType() { super();}

    public PetType(String petType) {
        this.petType = petType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    @Override
    public String toString() {
        return "PetTypes [id=" + id + ", type=" + petType + "]";
    }
}
