package com.expeditors.demo2.domain;

import jakarta.persistence.*;

import java.util.List;

@Table(name = "breed")
@Entity
public class Breed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "type", length = 15)
    private String breedType;

    //@OneToMany(cascade = PERSIST)
    @OneToMany(targetEntity = PetType.class)
    //@JoinColumn
    //private Set<PetType> petTypes = new HashSet<>();
    private List<PetType> petTypeList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBreedType() {
        return breedType;
    }

    public void setBreedType(String breedType) {
        this.breedType = breedType;
    }

    public Breed() {}

    public Breed(String breedType) {
        this.breedType = breedType;
    }

    public List getPetTypeList() {
        return petTypeList;
    }

    public void setPetTypeList(List petTypeList) {
        this.petTypeList = petTypeList;
    }

    /*
    public void setPetTypes(Set<PetType> petType) {
        this.petType = petTypes;
    }
    */
    @Override
    public String toString() {
        return "Breed [id=" + id + ", type=" + breedType + "]";
    }
}
