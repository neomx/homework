package com.expeditors.petadoption;

import com.expeditors.petadoption.domain.Adopter;
import com.expeditors.petadoption.domain.Pet;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
        System.out.println("Hello and welcome!");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("week13-persistence-unit");
        EntityManager em = emf.createEntityManager();

        AdoptService service = new AdoptService(em);

        em.getTransaction().begin();
        // Create a new pet
        Pet pet1 = new Pet();
        pet1.setPetName("Leo");
        pet1.setPetType("TURTLE");

        // Store the pet
        em.persist(pet1);

        // Create pet list
        List<Pet> petlist = new ArrayList<>();
        petlist.add(pet1);

        // Create an adopter
        Adopter adopter = new Adopter();
        adopter.setFirstName("Jim");
        adopter.setLastName("Smith");
        adopter.setEmail("smithj@netscape.com");
        adopter.setPhoneNumber("503-111-2222");
        adopter.setAdoptionDate(LocalDate.now());
        adopter.setPetList(petlist);

        // Store the adopter
        em.persist(adopter);
        em.getTransaction().commit();

        em.close();
        emf.close();
    }
}