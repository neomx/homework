package com.expeditors.petadoption;

import com.expeditors.petadoption.domain.Adopter;
import jakarta.persistence.EntityManager;


public class AdoptService {
    private EntityManager em;

    public AdoptService(EntityManager em) {
        this.em = em;
    }

    public Adopter createAdopter(Adopter adopter) {
        em.persist(adopter);
        return adopter;
    }

    public void removeAdopter(int id) {
        Adopter adopter = em.find(Adopter.class, id);
        if (adopter != null)
            em.remove(adopter);
    }

    public Adopter findAdopter(int id) {
        return em.find(Adopter.class, id);
    }
}
