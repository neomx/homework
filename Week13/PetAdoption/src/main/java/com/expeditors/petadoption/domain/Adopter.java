package com.expeditors.petadoption.domain;

import jakarta.persistence.*;
import java.util.List;
import java.time.LocalDate;

@Table(name = "adopter")
@Entity
public class Adopter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "adoption_date")
    private LocalDate adoptionDate;

    public  Adopter() {};

    public Adopter(int id, String firstName, String lastName, String email, String phoneNumber, LocalDate adoptionDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.adoptionDate = adoptionDate;
    }

    @OneToMany(targetEntity = Pet.class)
    private List<Pet> petList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getAdoptionDate() {
        return adoptionDate;
    }

    public void setAdoptionDate(LocalDate adoptionDate) {
        this.adoptionDate = adoptionDate;
    }

    public List getPetList() {
        return petList;
    }

    public void setPetList(List petList) {
        this.petList = petList;
    }

    @Override
    public String toString() {
        return "Adopter [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", phone=" + phoneNumber + ", adoptionDate=" + adoptionDate + "]";
    }
}
