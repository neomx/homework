package com.expeditors.petadoption.domain;

import jakarta.persistence.*;

@Table(name = "pet")
@Entity
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "pet_type")
    private String petType;

    @Column(name = "pet_name")
    private String petName;

    public Pet() { super();}

    public Pet(int id, String petType, String petName) {
        this.id = id;
        this.petType = petType;
        this.petName = petName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    @Override
    public String toString() {
        return "Pet [id=" + id + ", type=" + petType + ", name=" + petName + "]";
    }
}
