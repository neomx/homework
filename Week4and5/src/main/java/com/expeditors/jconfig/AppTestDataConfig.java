package com.expeditors.jconfig;

import org.springframework.stereotype.Component;
import com.expeditors.domain.Adopter;
import com.expeditors.domain.Pet;
import java.time.LocalDate;

@Component
public class AppTestDataConfig {
    public Adopter adopter1() {
        Adopter adopter = new Adopter();
        adopter.setId(1);
        adopter.setName("Steve");
        adopter.setPhoneNumber("503-134-8786");
        adopter.setAdoptionDate(LocalDate.now());
        Pet pet = new Pet(Pet.PetType.CAT, "Sparky", "Siamese");
        adopter.setPet(pet);
        return adopter;
    }

    public Adopter adopter2() {
        Adopter adopter = new Adopter();
        adopter.setId(2);
        adopter.setName("Marry");
        adopter.setPhoneNumber("503-863-0908");
        adopter.setAdoptionDate(LocalDate.now());
        Pet pet = new Pet(Pet.PetType.DOG, "Elmo", "Poodle");
        adopter.setPet(pet);
        return adopter;
    }

}
