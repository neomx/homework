package com.expeditors.jconfig;

import com.expeditors.dao.AdopterDAO;
import com.expeditors.dao.inmemory.InMemoryAdopterDAO;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import com.expeditors.domain.Adopter;
import com.expeditors.dao.BaseDAO;

@Configuration
@PropertySource({"classpath:/backend.properties"})
@ComponentScan("com.expeditors")
public class AppConfig {
    private AppTestDataConfig testDataProducer = new AppTestDataConfig();

    @Bean
    public AdopterDAO adopterDAO() {
        var dao = new InMemoryAdopterDAO();
        dao.insert(testDataProducer.adopter1());
        dao.insert(testDataProducer.adopter2());
        return dao;
    }
}
