package com.expeditors.app;

import java.util.List;
import com.expeditors.domain.Adopter;
import com.expeditors.jconfig.AppConfig;
import com.expeditors.service.AdopterService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AdopterMain {
    public static void main(String[] args) {
        AdopterMain adopterMain = new AdopterMain();
        adopterMain.goAdopter();
    }

    public void goAdopter() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("development");
        context.scan("com.expeditors");
        context.refresh();

        AdopterService as = context.getBean("adopterService", AdopterService.class);

        List<Adopter> adopters = as.getAllAdopters();
        System.out.println("Initial size: " + adopters.size());
        System.out.println(adopters);

        System.out.println();
        System.out.println("After delete one adopter");
        boolean adopters1 = as.deleteAdopter(1);

        List<Adopter> adopters2 = as.getAllAdopters();
        System.out.println("New size:" + adopters2.size());
        System.out.println(adopters2);

    }
}
